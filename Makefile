# By: Bruce W. Lowther
# License: GPL 3.0

## example invoke for testing
##
## TFILENAME=./data/sample01.tsv make test
##
## ?= set if not set already
##
TFILENAME ?= ./data/sample01.tsv

CC = g++

#CFLAGS_DEBUG = -g
#CFLAGS_OPTIMIZE = -Os

##debug and no optimize.
CFLAGS_DEBUG = -ggdb
CFLAGS_OPTIMIZE = -Og

##name the target after the directory.
TARGET = ewmaMain

# Object files: will find all .c/.h files in current directory
#  and in LIBDIR.  If you have any other (sub-)directories with code,
#  you can add them in to SOURCES below in the wildcard statement.
SOURCES=$(wildcard *.c $(LIBDIR)/*.c)
OBJECTS=$(SOURCES:.c=.o)
HEADERS=$(SOURCES:.c=.h)

##CFLAGS = -std=gnu99 -Wall
CFLAGS = -Wall

## add options for debugging and optimization.
CFLAGS += $(CFLAGS_DEBUG)
CFLAGS += $(CFLAGS_OPTIMIZE)


## Use short (8-bit) data types avr compatible stuff.
CFLAGS += -funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums 

ewmaMain: ewmaMain.c ewmaMain.h ewma.o noiseSignal.hpp
ewma.o: ewma.cpp ewma.hpp

$(TARGET): $(TARGET).h


test: $(TARGET)
	$(TARGET) $(TFILENAME)

debug: $(TARGET)
	gdb $(TARGET) $(TFILENAME)


#Tidy
clean:
	-rm $(TARGET)
	-rm *~
	-rm *.map
	-rm *.o

xplot:  $(TARGET) $(TFILENAME)
	$(TARGET) $(TFILENAME) > /tmp/foo.tsv
	gnuplot -e "filename='/tmp/foo.tsv';load 'do_gnuplot.gp'"
