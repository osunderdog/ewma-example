#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "avr_types.h"
#include "ewma.hpp"
#include "noiseSignal.hpp"

//resolve input handle from arg or default to stdin.
FILE *getInputHandle(int argc, char *argv[]);
  
