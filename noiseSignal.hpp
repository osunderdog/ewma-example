#ifndef noiseSignal_h
#define noiseSignal_h

#include <stdio.h>
#include "avr_types.h"
#include "ewma.hpp"

#define ABS_DELTA(x,y) ( ((x > y)?(x - y):(y - x)) );

template<class E> class NoiseSignal
{
private:
  E _highEWMA;
  E _middleEWMA;
  E _lowEWMA;
  
public:
  NoiseSignal();
  NoiseSignal(uint8_t,
	      uint8_t,
	      uint8_t);

  void update(const uint16_t);

  uint16_t getHighValue(void);
  uint16_t getMiddleValue(void);
  uint16_t getLowValue(void);
  uint16_t getNoise(void);
};


template<class E>
NoiseSignal<E>::NoiseSignal ():
  _highEWMA(4),
  _middleEWMA(4),
  _lowEWMA(4)
{
  
}

template<class E>
NoiseSignal<E>::NoiseSignal (uint8_t highFrac,
			     uint8_t middleFrac,
			     uint8_t lowFrac):
  _highEWMA(highFrac),
  _middleEWMA(middleFrac),
  _lowEWMA(lowFrac)
{
}

template<class E>
uint16_t NoiseSignal<E>::getHighValue(void)
{
  return _highEWMA.result();
}

template<class E>
uint16_t NoiseSignal<E>::getMiddleValue(void)
{
  return _middleEWMA.result();
}

template<class E>
uint16_t NoiseSignal<E>::getLowValue(void)
{
  return _lowEWMA.result();
}

template<class E>
uint16_t NoiseSignal<E>::getNoise(void)
{
  //always positive noise.
  //SHOULD always be positive anyway
  return ( ((_highEWMA.result() > _lowEWMA.result())?
	    (_highEWMA.result() - _lowEWMA.result()):
	    (_lowEWMA.result() - _highEWMA.result())) );
}

template<class E>
void NoiseSignal<E>::update(const uint16_t value)
{
  //keep a middle value
  _middleEWMA.update(value);
  
  //if value is greater than middle value then add it to the EWMA for the high value
  if(value > _middleEWMA)
  {
    _highEWMA.update(value);
  }
 
  if(value < _middleEWMA)
  {
    _lowEWMA.update(value);
  }  
}


#endif //#define noiseSignal_h
