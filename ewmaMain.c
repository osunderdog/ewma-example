#include "ewmaMain.h"

// Goal: pipe in data voltage values.
// Translate into on/off signal

int main(int argc, char *argv[]) {

  NoiseSignal<EWMA> ns(3,6,3);
  //NoiseSignal ns;

  //resolve filehandle for input
  FILE *fh = getInputHandle(argc, argv);
  
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  
  uint16_t adcValue;

  printf("adcValue\tHigh\tMiddle\tLow\tNoise\n");

  //assume each line contains a single integer number.
  while((read = getline(&line, &len, fh)) != -1) {
    //remove the CRLF at the end of the line (delimiter)
    //line[read-1]=0;
    
    adcValue = atoi(line);
    ns.update(adcValue);
    
    // --------------------------------------------------------------------------------
    // --------------------------------------------------------------------------------

    printf("%u\t%u\t%u\t%u\t%u\n",
	   adcValue,
	   ns.getHighValue(),
	   ns.getMiddleValue(),
	   ns.getLowValue(),
	   ns.getNoise()
	   );
  }
  
  //Done with the line buffer created in getline.
  free(line);
  
  return(EXIT_SUCCESS);
}


//resolve input handle from arg or default to stdin.
FILE *getInputHandle(int argc, char *argv[]) {

  //Step 1: read each line from stdin. this will simulate AVR voltage reading.
  FILE *fh;

  if (argc > 1) {
    if(!strcmp(argv[1], "-")) {
      //convention of dash meaning take input from stdin.
      fh = stdin;
    } else {
      fh = fopen(argv[1], "r");
      if(NULL == fh) {
	fprintf(stderr, "Unable to open [%s]:%s\n",
		argv[1],
		strerror(errno));
	exit(EXIT_FAILURE);
      }
    }
    
  } else {
    //otherwise assume it's coming from stdin.
    fh = stdin;
  }
  return(fh);
}
